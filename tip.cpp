#include <iostream>

int main()
{
  double  x, y, price, tip;

  std::cout << "Enter the price (pounds): ";
  std::cin >> x;

  std::cout << "Enter the tip percentage: ";
  std::cin >> y;

  tip = x/100 * y;
  price = tip + x;

  std::cout << "The tip is £" << tip << "\n";
  std::cout << "The total amount to pay is £" << price << "\n";

  return 0;
}
