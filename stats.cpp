#include <iostream>

int main()
{
  int x, y, sum, diff, product, distance;
  double mean;

  std::cout << "Enter two numbers: ";
  std::cin >> x >> y;

  sum = x + y;
  diff = x - y;
  product = x * y;
  distance = y - x;
  mean = sum / 2.0;

  std::cout << "The sum of " << x << " and " << y << " is " << sum << "\n";
  std::cout << "The difference between " << x << " and " << y << " is " << diff << "\n";
  std::cout << "The product of " << x << " and " << y << " is " << product << "\n";
  std::cout << "The distance between " << x << " and " << y << " is " << distance << "\n";
  std::cout << "The mean average of " << x << " and " << y << " is " << mean << "\n";

  return 0;
}
